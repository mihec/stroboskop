# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://mihec@bitbucket.org/mihec/stroboskop.git
```

Naloga 6.2.3:
https://bitbucket.org/mihec/stroboskop/commits/8bdf0c5e94f527d7ba98cc91e855f093e6c6651f

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/mihec/stroboskop/commits/ce88eb1f8feb9be627acbc658e85729e6942b01e

Naloga 6.3.2:
https://bitbucket.org/mihec/stroboskop/commits/9dcd995c02e247b78d5124a6c0774b2ce9eaf4bc

Naloga 6.3.3:
https://bitbucket.org/mihec/stroboskop/commits/8c31cac5e3d23f6dc0231ef61d37c5d608b95752

Naloga 6.3.4:
https://bitbucket.org/mihec/stroboskop/commits/5d78801ded83903511297058a713b4b95a8974c3

Naloga 6.3.5:

```
git checkout master
git merge izgled
git push origin master
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/mihec/stroboskop/commits/cc19ead04312433dd7ae164d94a295882cc03fef?at=dinamika

Naloga 6.4.2:
https://bitbucket.org/mihec/stroboskop/commits/1a69decbe6bdfaab1c2c2dc11a78250d4182072c

Naloga 6.4.3:
https://bitbucket.org/mihec/stroboskop/commits/afe7d2ed5ae23825b8fea5d3334b50a09ed4f6c7

Naloga 6.4.4:
https://bitbucket.org/mihec/stroboskop/commits/7e60112a02748f15307bc95ec1c72b8d448435e5